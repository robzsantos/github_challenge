package com.br.githubchallenge

import com.br.githubchallenge.uidata.GithubKotlinProject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class KotlinProjectsPresenter @Inject constructor() {

    var page: Int = 1

    @Inject
    internal lateinit var interactor: KotlinProjectsInteractor

    private var view: KotlinProjectsView? = null

    private var disposable: Disposable? = null

    fun attachView(view: KotlinProjectsView) {
        this.view = view
    }

    fun detachView() {
        this.view = null

        disposable?.dispose()

        disposable = null
    }

    fun getProjects() {
        if (page == 1)
            view?.startLoading()

        disposable = interactor.getList(page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ it ->

                val list = it.list.map {
                    GithubKotlinProject.from(it)
                }

                view?.setProjects(list)

                if (page == 1)
                    view?.endLoading()

                page++

            }) { throwable ->

                if (page == 1)
                    view?.endLoading()

                val message: String? = throwable.message

                view?.showError(message)
            }
    }

}