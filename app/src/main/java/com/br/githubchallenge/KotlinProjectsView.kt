package com.br.githubchallenge

import com.br.githubchallenge.uidata.GithubKotlinProject


interface KotlinProjectsView {

    fun showError(message: String?)

    fun startLoading()

    fun endLoading()

    fun setProjects(projects: List<GithubKotlinProject>, savedState: Boolean = false)
}