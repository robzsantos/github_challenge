package com.br.githubchallenge.di.modules

import com.br.githubchallenge.BuildConfig
import com.br.githubchallenge.di.Default
import com.br.githubchallenge.di.Github
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RetrofitModule {

    private fun basicBuilder(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    @Provides
    @Singleton
    @Github
    fun providesGithubRetrofit(@Default client: OkHttpClient, gson: Gson): Retrofit {
        return basicBuilder(gson)
            .baseUrl(BuildConfig.GITHUB_API)
            .client(client)
            .build()
    }

}