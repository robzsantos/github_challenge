package com.br.githubchallenge.di.modules

import com.br.githubchallenge.webservice.GithubApi
import com.br.githubchallenge.di.Github
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApisModule {

    @Provides
    @Singleton
    fun providesGithubApi(@Github retrofit: Retrofit): GithubApi =
        retrofit.create(GithubApi::class.java)

}