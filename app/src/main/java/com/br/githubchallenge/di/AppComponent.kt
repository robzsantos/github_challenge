package com.br.githubchallenge.di

import com.br.githubchallenge.KotlinProjectsActivity
import com.br.githubchallenge.di.modules.*
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AndroidModule::class, RetrofitModule::class, ApisModule::class, OkHttpClientModule::class, GsonModule::class])
@Singleton
interface AppComponent {

    fun inject(kotlinProjectsActivity: KotlinProjectsActivity)

}