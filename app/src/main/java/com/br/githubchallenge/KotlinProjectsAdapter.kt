package com.br.githubchallenge

import com.airbnb.epoxy.EpoxyAdapter
import com.br.githubchallenge.uidata.GithubKotlinProject
import com.br.githubchallenge.utils.listview.LoadingModel

class KotlinProjectsAdapter : EpoxyAdapter() {

    private val loadingModel = LoadingModel()

    fun addProjects(projects: List<GithubKotlinProject>) {
        removeModel(loadingModel)

        addModels(
            projects.map { KotlinProjectModel(it) }.toList()
        )

        addModel(loadingModel)

    }

}