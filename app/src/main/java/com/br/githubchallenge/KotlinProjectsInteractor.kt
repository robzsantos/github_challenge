package com.br.githubchallenge

import com.br.githubchallenge.webservice.GithubApi
import javax.inject.Inject

class KotlinProjectsInteractor @Inject constructor() {

    private val QUERY = "language:kotlin"

    private val SORT = "stars"

    @Inject
    lateinit var githubApi: GithubApi

    fun getList(page: Int) = githubApi.getList(language = QUERY, sort = SORT, page = page)

}