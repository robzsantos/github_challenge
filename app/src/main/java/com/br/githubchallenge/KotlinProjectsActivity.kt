package com.br.githubchallenge

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.br.githubchallenge.di.AppComponent
import com.br.githubchallenge.extensions.error
import com.br.githubchallenge.uidata.GithubKotlinProject
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.progress_bar_loading.*
import java.io.Serializable
import javax.inject.Inject

class KotlinProjectsActivity : AppCompatActivity(), KotlinProjectsView {

    private val appComponent: AppComponent
        get() {
            val application = application as GithubChallengeApplication

            return application.appComponent ?: throw IllegalStateException()
        }

    @Inject
    lateinit var presenter: KotlinProjectsPresenter

    private var adapter: KotlinProjectsAdapter? = null

    private var projects: ArrayList<GithubKotlinProject> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        appComponent.inject(this)

        presenter.attachView(this)

        setupRecyclerView()

        savedInstanceState?.let {
            with(it) {
                projects = this.getSerializable(PROJECTS) as ArrayList<GithubKotlinProject>
                presenter.page = this.getInt(PAGE)
            }
        }

        if (projects.isEmpty())
            presenter.getProjects()
        else {
            setProjects(projects, true)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(PROJECTS, projects as Serializable)
        outState.putInt(PAGE, presenter.page)
    }

    private fun setupRecyclerView() {
        recycler_view_kotlin_projects.layoutManager = LinearLayoutManager(this)

        adapter = KotlinProjectsAdapter()

        recycler_view_kotlin_projects.adapter = adapter

        recycler_view_kotlin_projects.onNextLoad { _, _ ->
            presenter.getProjects()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.detachView()
    }

    override fun setProjects(projects: List<GithubKotlinProject>, savedState: Boolean) {
        adapter?.addProjects(projects)

        if (!savedState)
            this.projects.addAll(projects)
    }

    override fun showError(message: String?) {
        error(message)
    }

    override fun startLoading() {
        constraint_layout_loading_frame.visibility = View.VISIBLE
    }

    override fun endLoading() {
        constraint_layout_loading_frame.visibility = View.GONE
    }

    companion object {
        private const val PROJECTS = "projects"
        private const val PAGE = "page"
    }
}