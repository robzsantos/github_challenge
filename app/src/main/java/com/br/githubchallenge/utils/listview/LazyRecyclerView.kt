package com.br.githubchallenge.utils.listview

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView

class LazyRecyclerView : RecyclerView,
    PaginationManager.Callback {

    private var paginationManager: PaginationManager? = null

    private var callback: ((Int, Int) -> Unit)? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init()
    }

    private fun init() {
        isNestedScrollingEnabled = false

        setHasFixedSize(true)
    }

    override fun setLayoutManager(layout: LayoutManager?) {
        super.setLayoutManager(layout)

        paginationManager = PaginationManager()

        paginationManager?.setupWithRecyclerView(this, this)
    }

    fun onNextLoad(callback: (Int, Int) -> Unit) {
        this.callback = callback
    }

    override fun onLoadMore(currentPage: Int, totalItemCount: Int, recyclerView: RecyclerView) {
        callback?.invoke(currentPage, totalItemCount)
    }
}