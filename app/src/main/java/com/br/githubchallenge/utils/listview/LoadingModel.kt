package com.br.githubchallenge.utils.listview

import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyModel
import com.br.githubchallenge.R

class LoadingModel : EpoxyModel<ConstraintLayout>() {

    override fun getDefaultLayout() = R.layout.item_progress_bar_loading

}