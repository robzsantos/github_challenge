package com.br.githubchallenge

import androidx.constraintlayout.widget.ConstraintLayout
import com.br.githubchallenge.uidata.GithubKotlinProject
import com.br.githubchallenge.utils.BinderEpoxyModel
import com.br.githubchallenge.utils.GlideApp
import kotlinx.android.synthetic.main.item_kotlin_project.view.*

class KotlinProjectModel(private val kotlinProject: GithubKotlinProject) :
    BinderEpoxyModel<ConstraintLayout>() {

    override fun getDefaultLayout() = R.layout.item_kotlin_project

    override fun bind(view: ConstraintLayout) {
        super.bind(view)

        val context = view.context

        with(kotlinProject) {
            GlideApp.with(view.context)
                .load(ownerAvatar)
                .into(view.imageview_avatar_item_kotlin_project)

            view.textview_name_item_kotlin_project.text = name

            view.textview_info_item_kotlin_project.text =
                context?.getString(R.string.item_kotlin_project_info, rating, forks)

            view.textview_owner_item_kotlin_project.text =
                context?.getString(R.string.item_kotlin_project_owner, ownerName)
        }
    }
}