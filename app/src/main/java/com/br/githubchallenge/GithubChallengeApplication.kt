package com.br.githubchallenge

import android.app.Application
import com.br.githubchallenge.di.AppComponent
import com.br.githubchallenge.di.DaggerAppComponent
import com.br.githubchallenge.di.modules.AndroidModule

class GithubChallengeApplication : Application() {

    var appComponent: AppComponent? = null
        private set

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .androidModule(AndroidModule(this))
            .build()
    }
}