package com.br.githubchallenge.uidata

import com.br.githubchallenge.webservice.ProjectResponse
import java.io.Serializable

class GithubKotlinProject : Serializable {

    var name: String? = null

    var rating: Long? = null

    var forks: Long? = null

    var ownerName: String? = null

    var ownerAvatar: String? = null

    companion object {

        fun from(kotlinProject: ProjectResponse): GithubKotlinProject =
            GithubKotlinProject().apply {
                this.name = kotlinProject.name
                this.rating = kotlinProject.score
                this.forks = kotlinProject.forks
                this.ownerName = kotlinProject.owner.name
                this.ownerAvatar = kotlinProject.owner.avatar
            }
    }
}